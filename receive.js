let amqp = require('amqplib/callback_api');
const nodemailer = require("nodemailer");

amqp.connect('amqp://localhost', function(error0, connection) {
    if (error0) {
        throw error0;
    }
    connection.createChannel(function(error1, channel) {
        if (error1) {
            throw error1;
        }
        let queue = 'hello';
        let transporter = nodemailer.createTransport({
            service: 'gmail',
            host: "smtp.gmail.com",
            auth: {
                user: "testmail@gmail.com", // generated ethereal user
                pass: "123", // generated ethereal password
            },
        });

        console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", queue);
        channel.consume(queue, async function (msg) {
            // Message object
            let datas = JSON.parse(msg.content)
            let message = {
                // sender info
                from: 'App Notif <testmail@gmail.com>',
                to: datas.email.toString(),
                // Subject of the message
                subject: 'Your user is created',

                // HTML body
                html: `<html>Username : ${datas.username}<br>Password : ${datas.password}</html>`,
            };
            await transporter.sendMail(message);
            console.log(" [x] Received %s", JSON.stringify(datas).toString());
        }, {
            noAck: true
        });
    });
});